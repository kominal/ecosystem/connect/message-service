# Message Service

The message service is responsible for keeping track of the messages which have been sent in every group.

## Documentation

Production: https://connect.kominal.com/message-service/api-docs
Test: https://connect-test.kominal.com/message-service/api-docs
