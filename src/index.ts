import Service from '@kominal/service-util/helper/service';
import get from './routes/get';
import post from './routes/post';
import group from './routes/delete';
import { SMQHandler } from './handler.smq';

const service = new Service({
	id: 'message-service',
	name: 'Message Service',
	description: 'Manages messages in a group and distributes them.',
	jsonLimit: '16mb',
	routes: [get, post, group],
	database: true,
	swarmMQ: new SMQHandler(),
});
service.start();

export default service;
