import { verifyParameter } from '@kominal/service-util/helper/util';
import { SERVICE_TOKEN, STACK_NAME, TIMEOUT } from '@kominal/service-util/helper/environment';
import MessageDatabase from '@kominal/connect-models/message/message.database';
import axios from 'axios';
import Router from '@kominal/service-util/helper/router';
import { error } from '@kominal/service-util/helper/log';

const router = new Router();

/**
 * Deletes all messages in a group.
 * @group Private
 * @security TOKEN
 * @route POST /{groupId}/all
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The groupId for which all messages should be deleted.
 * @returns {void} 200 - The messages to delete.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.deleteAsRoot(':groupId/all', async (req, res) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	//TODO: Check permission in storage service
	const messages = await MessageDatabase.find({ groupId });
	const storageIds = messages.map((message) => message.get('storageId'));
	await MessageDatabase.deleteMany({ groupId });

	try {
		await axios.post(
			`http://${STACK_NAME}_storage-service:3000/remove`,
			{ ids: storageIds },
			{
				headers: {
					authorization: SERVICE_TOKEN,
				},
				timeout: TIMEOUT,
			}
		);
	} catch (e) {
		error(e);
	}

	res.status(200).send();
});

export default router.getExpressRouter();
