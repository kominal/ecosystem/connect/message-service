import { verifyParameter } from '@kominal/service-util/helper/util';
import { SERVICE_TOKEN, STACK_NAME, TIMEOUT } from '@kominal/service-util/helper/environment';
import MessageDatabase from '@kominal/connect-models/message/message.database';
import axios from 'axios';
import Router from '@kominal/service-util/helper/router';
import { Document } from 'mongoose';
import { MembershipEncrypted } from '@kominal/connect-models/membership/membership.encrypted';

const router = new Router();

/**
 * Gets one single message object
 * @group Protected
 * @security JWT
 * @route GET /{messageId}
 * @consumes application/json
 * @produces application/json
 * @param {string} messageId.body.required - The message id of the message to get
 * @returns {MessageEncrypted} 200 - The encrypted message
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/:messageId', async (req, res, userId) => {
	const { messageId } = req.params;
	verifyParameter(messageId);

	const message = await MessageDatabase.findById(messageId);

	if (!message) {
		throw 'error.message.notfound';
	}

	let member: MembershipEncrypted;

	try {
		const response = await axios.get<MembershipEncrypted>(
			`http://${STACK_NAME}_group-service:3000/members/${message.get('groupId')}/time`,
			{
				headers: {
					authorization: SERVICE_TOKEN,
					userId,
				},
				timeout: TIMEOUT,
			}
		);
		member = response.data;
	} catch (e) {
		throw new Error('error.server.error');
	}

	if (!member) {
		throw 'error.group.notmember';
	}

	if (member.joined < message.get('time')) {
		throw 'error.message.notfound';
	}

	if (member.left > message.get('time')) {
		throw 'error.message.notfound';
	}

	res.status(200).send({
		id: message._id,
		userId: message.get('userId'),
		time: message.get('time'),
		groupId: message.get('groupId'),
		storageId: message.get('storageId'),
	});
});

/**
 * Returns the a sequence of messages sorted by time starting from the known message going in the PAST or FUTURE.
 * If the known message is not provided or the gap between the latest message and the known is greater or equals to 1000
 * the client is asked to purge the local database within the response.
 * @group Protected
 * @security JWT
 * @route GET /{groupId}/{direction}/{knownMessageId}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.body.required - The group id of the group the messages should be loaded from.
 * @param {string} direction.body.required - The direction of the flow. PAST or FUTURE
 * @param {string} knownMessageId.body.optional - The message from where to start the flow.
 * @returns {object} 200 - The message flow
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/:groupId/:direction/:knownMessageId*?', async (req, res, userId) => {
	const { groupId, direction, knownMessageId } = req.params;
	verifyParameter(groupId);

	let member: MembershipEncrypted;

	try {
		const response = await axios.get<MembershipEncrypted>(`http://${STACK_NAME}_group-service:3000/members/${groupId}/time`, {
			headers: {
				authorization: SERVICE_TOKEN,
				userId,
			},
			timeout: TIMEOUT,
		});
		member = response.data;
	} catch (e) {
		throw new Error('error.server.error');
	}

	if (!member) {
		throw 'error.group.notmember';
	}

	const userTimeConditions: any[] = [];
	if (member.joined) {
		userTimeConditions.push({ time: { $gt: member.joined } });
	}
	if (member.left) {
		userTimeConditions.push({ time: { $lt: member.left } });
	}

	const knownMessageTimeConditions: any[] = [...userTimeConditions];
	if (knownMessageId != null) {
		const knownMessage = await MessageDatabase.findOne({ _id: knownMessageId, groupId });
		if (knownMessage != null) {
			if (direction === 'PAST') {
				knownMessageTimeConditions.push({ time: { $lt: knownMessage.get('time') } });
			} else if (direction === 'FUTURE') {
				knownMessageTimeConditions.push({ time: { $gt: knownMessage.get('time') } });
			}
		}
	}

	let purge = false;
	let messages: Document[] = [];

	if (
		direction === 'FUTURE' &&
		(await MessageDatabase.find({
			groupId,
			$and: knownMessageTimeConditions,
		}).estimatedDocumentCount()) > 1000
	) {
		purge = true;
		messages = await MessageDatabase.find({
			groupId,
			$and: userTimeConditions,
		})
			.sort({ time: 1 })
			.limit(1000);
	} else {
		messages = await MessageDatabase.find({
			groupId,
			$and: knownMessageTimeConditions,
		})
			.sort({ time: direction === 'PAST' ? -1 : 1 })
			.limit(1000);
	}

	res.status(200).send({
		purge,
		messages: messages.map((message) => {
			return {
				id: message._id,
				userId: message.get('userId'),
				time: message.get('time'),
				groupId: message.get('groupId'),
				storageId: message.get('storageId'),
			};
		}),
	});
});

export default router.getExpressRouter();
