import { verifyParameter } from '@kominal/service-util/helper/util';
import { SERVICE_TOKEN, STACK_NAME, TIMEOUT } from '@kominal/service-util/helper/environment';
import { notify } from '@kominal/service-util/interfaces/notification';
import MessageDatabase from '@kominal/connect-models/message/message.database';
import axios from 'axios';
import Router from '@kominal/service-util/helper/router';
import service from '..';

const router = new Router();

/**
 * Send a message to a group.
 * @group Protected
 * @security JWT
 * @route POST /send
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.body.required - The group the message should be sent to.
 * @param {string} storageId.body.required - The storage id
 * @returns {void} 200 - TODO
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/', async (req, res, userId) => {
	const { groupId, storageId } = req.body;
	verifyParameter(groupId, storageId);

	let members: string[] = [];

	try {
		const response = await axios.get<string[]>(`http://${STACK_NAME}_group-service:3000/members/${groupId}/current`, {
			headers: {
				authorization: SERVICE_TOKEN,
			},
			timeout: TIMEOUT,
		});
		members = response.data;
	} catch (e) {
		throw new Error('error.server.error');
	}

	if (!members || members.indexOf(userId) === -1) {
		throw 'error.group.notmember';
	}

	const message = await MessageDatabase.create({
		userId,
		groupId,
		time: new Date(),
		storageId,
	});

	service.getSMQClient()?.publish('QUEUE', `INTERNAL.NOTIFICATION`, 'MESSAGE.NEW', { userIds: members, groupId, messageId: message._id });

	try {
		await axios.post(
			`http://${STACK_NAME}_group-service:3000/activity/${groupId}`,
			{},
			{
				headers: {
					authorization: SERVICE_TOKEN,
				},
				timeout: TIMEOUT,
			}
		);
	} catch (e) {
		throw new Error('error.server.error');
	}

	res.status(200).send({
		id: message._id,
		userId: message.get('userId'),
		groupId: message.get('groupId'),
		storageId: message.get('storageId'),
		time: message.get('time'),
	});
});

export default router.getExpressRouter();
